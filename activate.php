<?php include_once 'include/function.php'; ?>
<!DOCTYPE html>
<html lang="en">


<head>

    <meta charset="utf-8" />
    <title>Activation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.png">

    <!-- Template CSS Files -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/select2.min.css">
    <link rel="stylesheet" href="css/style.css">
	

    <!-- Template JS Files -->
    <script src="js/modernizr.js"></script>

</head>

<body class="auth-page light">
    <!-- Wrapper Starts -->
    <div class="wrapper">
        <div class="container user-auth">
			<div class="hidden-xs col-sm-4 col-md-4 col-lg-4">
				<!-- Logo Starts -->
				<a class="logo" href="./">
					<img id="logo-user" class="img-responsive" src="images/logo-dark.png" alt="logo">
				</a>
				<!-- Logo Ends -->
				<!-- Slider Starts -->
				
				<!-- Slider Ends -->
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<!-- Logo Starts -->
				<a class="visible-xs" href="./">
					<img id="logo" class="img-responsive mobile-logo" width="200" src="images/logo-dark.png" alt="logo">
				</a>
				<!-- Logo Ends -->
				<div class="form-container">
					<div>
						<!-- Section Title Starts -->
						<div class="row text-center">
							<h2 class="title-head hidden-xs">member <span>Activation</span></h2>
							 <p class="info-form">Account Activation</p>
						</div>
						<!-- Section Title Ends -->
						<!-- Form Starts -->
						<div class="text-center">
							<?php Activate(); echo Error_Message(); echo Success_Message(); ?>
						</div>
						<!-- Form Ends -->
					</div>
				</div>
			</div>
		</div>
        <!-- Template JS Files -->
        <script src="js/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/select2.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/custom.js"></script>
		
		<!-- Live Style Switcher JS File - only demo -->
		<script src="js/styleswitcher.js"></script>

    </div>
    <!-- Wrapper Ends -->
</body>

</html>