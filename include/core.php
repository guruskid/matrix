<?php

// Query Function
function Query($query) {
    global $conn;
    return mysqli_query($conn, $query);
    confirm($query);
}

function fetch_all($result) {
    global $conn;
    return mysqli_fetch_array($result);
}

function escape($string) {
    global $conn;
    return mysqli_real_escape_string($conn, $string);
} 

function row_count($result) {
    global $conn;
    return mysqli_num_rows($result);
}
// Redirect Location
function Redirect($New_Location) {
    header("Location:" . $New_Location);
    exit;
}

function clean($string) {
    global $conn;
    return htmlentities($string);
}

function confirm($result) {
    global $conn;
    if (!$result) {
        die('Query Failed' .mysqli_error($conn));
    }
}

/// Error Message Echo
function Error_Message() {
    if (isset($_SESSION['ErrorMessage'])) {
        $Output = "<div class='alert alert-danger'>";
        $Output .= htmlentities($_SESSION['ErrorMessage']);
        $Output .= "</div>";
        $_SESSION['ErrorMessage'] = null;
        return $Output;
    }
}

/// Success Message Echo
function Success_Message() {
    if (isset($_SESSION['SuccessMessage'])) {
        $Output = "<div class='alert alert-success'>";
        $Output .= htmlentities($_SESSION['SuccessMessage']);
        $Output .= "</div>";
        $_SESSION['SuccessMessage'] = null;
        return $Output;
    }
}

// token generator
function token_gen() {
    $token = $_SESSION['token'] = md5(uniqid(mt_rand(), true));
    return $token;
}

// send email
function sendMail($email, $subject, $message) {

    $mail = new PHPMailer;

    $mail->isSMTP();
    $mail->Host = 'cryptomatrix.co';
    $mail->SMTPAuth = true; 
    $mail->Username = 'support@cryptomatrix.co';
    $mail->Password = 'Password@2020';
    $mail->SMTPSecure = 'ssl';
    $mail->Mailer = "smtp";
    $mail->SMTPKeepAlive = true;
    $mail->Port = 465;

    $mail->setFrom('support@cryptomatrix.co', 'CRYPTO MATRIX');
    $mail->addAddress($email);
    $mail->isHTML(true);


    $mail->Subject = $subject;
    $mail->Body    = $message;

    if(!$mail->send()) {
        return false;
    } else {
        return true;
    }   
    // return mail($email, $subject, $message);
}


?>
